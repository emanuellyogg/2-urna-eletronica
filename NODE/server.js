const express = require("express");
const fs = require("fs");
const app = express();
const porta = 3001;
const cors = require('cors');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
app.use(express.static("public"));

app.listen(porta, function () {
  console.log(`Servidor está rodando na porta: ${porta}`);
});

app.get("/cargainicial", function (req, resp) {

  fs.readFile("config.csv", "utf8", function (err, data) {

    if (err) {
      console.log(`Erro ao ler arquivo: ${err}`);
    }

    let listaCand = exibirCandidatos(data);
    resp.send(listaCand);
  });
});

function exibirCandidatos(data) {
  let listaCandidatos = data.split("\r\n");

  let listaCand = [];

  for (let i = 0; i < listaCandidatos.length; i++) {
    const element = listaCandidatos[i];
    listaCand.push(element.split(","));
  }

  return listaCand;
}

app.post("/voto", function (req, resp) {

  let voto = `${req.body.rg},${req.body.numCand},${req.body.timestampVoto}`

  fs.appendFile("votacao.csv", `${voto}\n`, function (err) {
    if (err) {
      resp.json({
        "Status": "500",
        "mensagem": "Erro ao registrar voto, contate o administrador do sistema"
      }
      );
      throw err;
    } else {
      console.log("votação registrada com sucesso");
      resp.json({
        "Status": "200",
        "mensagem": "Voto Registrado Com sucesso"
      }
      );
    }
  });
});

app.get("/apuracao", function (req, resp) {

  var listaCandidatos = [];

  fs.readFile("config.csv", "utf8", function (err, data) {

    if (err) {
      console.log(`Erro ao ler arquivo: ${err}`);
    }

    listaCandidatos = exibirCandidatos(data);
  });

  fs.readFile("votacao.csv", "utf8", function (err, data) {
    if (err) {
      console.log(`Erro ao ler arquivo: ${err}`);
    }

    let listaVotos = data.split("\n");

    let listaTodosVotos = [];

    for (let i = 0; i < listaVotos.length; i++) {
      const element = listaVotos[i];
      listaTodosVotos.push(element.split(","));
    }

    for (let indexCand = 0; indexCand < listaCandidatos.length; indexCand++) {
      const numCand = listaCandidatos[indexCand][1];

      let votos = contarVotos(numCand, listaTodosVotos);

      listaCandidatos[indexCand][4] = votos;
    }
    resp.send(ordenarLista(listaCandidatos));
  });
});

function contarVotos(numCand, listaTodosVotos) {
  let totalVotos = 0;
  
  for (let indexVotos = 0; indexVotos < listaTodosVotos.length; indexVotos++) {

    if (listaTodosVotos[indexVotos][1] == numCand) {
      totalVotos ++
    }
  }
  return totalVotos;
}

function ordenarLista(list) {

  list.sort(function (a, b) {
    if (a[4] > b[4]) {
      return -1
    } else {
      return true;
    }
  });
  return list
}