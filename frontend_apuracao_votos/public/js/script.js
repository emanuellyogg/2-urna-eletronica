window.addEventListener("load", async function () {
  let listaApuracao = await getVotos();
  console.log(listaApuracao);
  montarListaApuracao(listaApuracao);
});

async function getVotos() {
  var response = await fetch("http://localhost:3001/apuracao");
  return response.json();
}

function montarListaApuracao(listaApuracao) {
  var votosUl = document.getElementById("ulVotos");

  for (let i = 0; i < listaApuracao.length; i++) {
    votosUl.appendChild(criarListaVotos(listaApuracao[i]));
  }
}

function criarListaVotos(candVotos) {
  let li = document.createElement("li");
  let span = document.createElement("span");
  let img = document.createElement("img");

  li.classList.add("list-group-item");
  img.classList.add("img-thumbnail");
  img.width = "80";
  img.height = "80";

  img.src = candVotos[3];
  span.textContent = " - " + candVotos[1] + " - " + candVotos[2] + " - " +  candVotos[4];

  li.appendChild(img);
  li.appendChild(span);

  return li;
}