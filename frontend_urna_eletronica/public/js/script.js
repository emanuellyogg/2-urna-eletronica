var votoCand11 = 0;
var votoCand12 = 0;
var votoCand13 = 0;
var votoBranco = 0;
var votoNulo = 0;
var listaCand = [];

window.addEventListener("load", async function () {
  listaCand = await getCand();
  montarListaCand(listaCand);
});

async function getCand() {
  var response = await fetch("http://localhost:3001/cargainicial");
  return response.json();
}

function montarListaCand(listCandi) {

  if (listCandi[0][0] == "a") {
    let campRG = document.getElementById("idRG");
    campRG.disabled = true;
  }

  var candUl = document.getElementById("ulCand")

  for (let i = 0; i < listCandi.length; i++) {
    candUl.appendChild(criarLista(listCandi[i]));
  }
}

function criarLista(candidato) {
  let li = document.createElement("li");
  let span = document.createElement("span");
  let img = document.createElement("img");

  li.classList.add("list-group-item");
  img.classList.add("img-thumbnail");
  img.width = "75";
  img.height = "75";

  img.src = candidato[3];
  span.textContent = " - " + candidato[1] + " - " + candidato[2];

  li.appendChild(img);
  li.appendChild(span);

  return li;
}

var btnVotar = document.getElementById("btn-confirm");
btnVotar.addEventListener("click", async function () {

  var timestamp = pegarHora();
  var numCandDigitado = Number(document.getElementById("idNumCand").value);

  if (numCandDigitado == "") {
    numCandDigitado = "Nulo";
  } else if (numCandInvalido(numCandDigitado)) {
    alert("Candidato inválido! Consulte a Lista de Candidatos");
    return;
  }

  const update = {
    rg: Number(document.getElementById("idRG").value),
    numCand: numCandDigitado,
    timestampVoto: timestamp
  };

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(update),
  };

  let response = await registrarVoto(options);

  exibirAlerta(response);
  limparTela();
});

function numCandInvalido(numCandDig) {

  for (let i = 0; i < listaCand.length; i++) {
    if (numCandDig == listaCand[i][1]) {
      return false;
    }
  }
  return true;
}

var btnVotarBranco = document.getElementById("btn-white");
btnVotarBranco.addEventListener("click", async function () {
  votarBranco();

  var timestamp = pegarHora();

  const update = {
    rg: Number(document.getElementById("idRG").value),
    numCand: "Branco",
    timestampVoto: timestamp
  };

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(update),
  };

  let response = await registrarVoto(options);

  exibirAlerta(response);
});

function votarBranco() {
  votoBranco++
  limparTela();
}

function exibirAlerta(response) {
  if (response.Status == "200") {

    let alertaSucesso = document.getElementById("alertaSucesso");
    alertaSucesso.style.display = "block";

    setTimeout(function () {
      alertaSucesso.style.display = "none"
    }, 2000);

  } else if (response.Status == "500") {

    let alertaErro = document.getElementById("alertaErro");
    alertaErro.style.display = "block";
    desabilitarTela();
  }
}

function desabilitarTela() {
  document.getElementById("idRG").disabled = true;
  document.getElementById("idNumCand").disabled = true;
  document.getElementById("btn-candidato").disabled = true;
  document.getElementById("btn-cancel").disabled = true;
  document.getElementById("btn-white").disabled = true;
  document.getElementById("btn-confirm").disabled = true;
}

var btnAlertaErro = document.getElementById("btnErro");
btnAlertaErro.addEventListener("click", function () {
  document.getElementById("alertaErro").style.display = "none";
  habilitarTela();
});

function habilitarTela() {
  document.getElementById("idRG").disabled = false;
  document.getElementById("idNumCand").disabled = false;
  document.getElementById("btn-candidato").disabled = false;
  document.getElementById("btn-cancel").disabled = false;
  document.getElementById("btn-white").disabled = false;
  document.getElementById("btn-confirm").disabled = false;
}

function pegarHora() {
  let timeAtual = new Date();

  let ano = timeAtual.getFullYear();
  let mes = (timeAtual.getMonth()) + 1;

  if (mes < 10) {
    mes = "0" + mes.toString();
  }

  let dia = timeAtual.getDate();

  if (dia < 10) {
    dia = "0" + dia.toString();
  }

  let hora = timeAtual.getHours();
  let minuto = timeAtual.getMinutes();
  let segundos = timeAtual.getSeconds();

  let dataCompleta = ano.toString() + mes.toString() + dia.toString() + hora.toString() + minuto.toString() + segundos.toString();

  return dataCompleta;
}

async function registrarVoto(options) {
  let response = await fetch("http://localhost:3001/voto", options);
  return response.json();
}

function contarVotosTela() {
  let voteSelect = document.querySelector("#idNumCand");
  voteSelect = Number(voteSelect.value);

  switch (voteSelect) {
    case 11:
      votoCand11++
      break;
    case 12:
      votoCand12++
      break;
    case 13:
      votoCand13++
      break;
    default:
      if (voteSelect == "") {
        votoNulo++
      }
      break;
  }
  showResult();
  limparTela();
}

function showResult() {
  let totalCand11 = document.querySelector("#showResCand11");
  totalCand11.innerHTML = `Votos Jiraiya Sensei: ${votoCand11}`;

  let totalCand12 = document.querySelector("#showResCand12");
  totalCand12.innerHTML = `Votos Kakashi Hatake: ${votoCand12}`;

  let totalCand13 = document.querySelector("#showResCand13");
  totalCand13.innerHTML = `Votos Shikamaru Nara: ${votoCand13}`;

  let totalBranco = document.querySelector("#showResWhite");
  totalBranco.innerHTML = `Votos em Branco: ${votoBranco}`;

  let totalNulo = document.querySelector("#showesNullo");
  totalNulo.innerHTML = `Votos Nulo: ${votoNulo}`;
}

var btnVisuCandidato = document.getElementById("btn-candidato");
btnVisuCandidato.addEventListener("click", function () {
  fotoCandidato();
});

function fotoCandidato() {
  let numCand = document.querySelector("#idNumCand");
  numCand = Number(numCand.value);
  let foto;
  let candidato;

  switch (numCand) {
    case 11:
      foto = "jiraiya.jpg";
      candidato = "Jiraiya Sensei";
      break;
    case 12:
      foto = "kakashi.jpg";
      candidato = "Kakashi Hatake";
      break;
    case 13:
      foto = "shikamaru.jpg";
      candidato = "Shikamaru Nara";
      break;
    default:
      if (numCand == "") {
        foto = "nulo.png";
        candidato = "Voto Nulo";
      }
      break;
  }

  let img = document.querySelector("#imgCandidate");
  img.src = "images/" + foto;

  let nomeCand = document.querySelector("#NmCandidato");
  nomeCand.value = candidato;
}

var btnLimparTela = document.getElementById("btn-cancel");
btnLimparTela.addEventListener("click", function () {
  limparTela();
});

function limparTela() {
  document.querySelector("#NmCandidato").value = "";
  document.querySelector("#imgCandidate").src = "images/candidato00.jpg";
  document.querySelector("#idNumCand").value = "";
}